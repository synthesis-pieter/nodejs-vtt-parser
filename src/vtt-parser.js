const HeaderLine = require('./vtt_class').headerLine
const Cue = require('./vtt_class').cue
const VttInfo = require('./vtt_class').vttInfo

let parseVTT = function(vttLines){
    // empty array
    if(vttLines.length === 0)
        return null

    // only empty lines
    if(vttLines.filter(line => line.length > 0).length === 0)
        return null

    cueArray = []; //array of cue objects
    for (let i = 0; i < vttLines.length; i++) {
        currentLine = vttLines[i];

        if (currentLine.match(/WEBVTT/) != null) {
            //console.log('WEBVTT found');
            var fileHeader = new HeaderLine(currentLine);
        } else 
            if (currentLine.match(/-->/) != null) {
                let startTime = currentLine.substring(0,currentLine.indexOf(" "));
                let stopTime = currentLine.substring(currentLine.indexOf('>')+2,currentLine.length);
                //console.log(currentLine.indexOf('/\r?\n/'));
                var cueObject = new Cue(startTime,stopTime,"");
            } else
                if (currentLine.match(/^\-/) != null) {
                    try {
                        cueObject.setDialogue(cueObject.getDialogue() + currentLine.substring(2,currentLine.length)+' ');
                    } catch {
                        //console.log('cueObject not defined');
                    }
                    
                } else {
                    //console.log('Found empty line');
                    if (typeof cueObject === 'object'){
                        cueArray.push(cueObject);
                        delete cueObject;
                    } 
                }

    }
    if (typeof cueObject === 'object'){
        cueArray.push(cueObject);
        delete cueObject;
    } 

    return new VttInfo(fileHeader,cueArray);
}

module.exports = parseVTT

