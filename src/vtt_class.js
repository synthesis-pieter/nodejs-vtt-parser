
/**
 * https://www.w3.org/TR/webvtt1/#model-cues
 */
class HeaderLine {
  header = "";
}

class Cue {
  
  constructor(startTime, stopTime, dialogue) {
    this.startTime = startTime;
    this.stopTime = stopTime;
    this.dialogue = dialogue;
  }

  getStartTime() { return this.startTime; }
  getStopTime() { return this.stopTime; }
  getDialogue() { return this.dialogue; }

  setStartTime(startTime) { this.startTime = startTime; }
  setStopTime(stopTime) { this.stopTime = stopTime; }
  setDialogue(dialogue) { this.dialogue = dialogue; }
}

class VttInfo {
  constructor(headerLine, cue) {
    this.headerLine = headerLine;
    this.cue = cue;
  }

  setHeaderLine(header) { this.headerLine = header; }
  setCue(cue) { this.cue = cue; }

  getHeaderLine() { return this.headerLine; }
  getCue(cue) { return this.cue; }
}

module.exports = { headerLine: HeaderLine, cue: Cue, vttInfo: VttInfo };