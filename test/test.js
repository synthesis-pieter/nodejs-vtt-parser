let parseVTT = require('../src/vtt-parser')
let assert = require('assert');
const fs = require('fs');
let vtt_cl = require('../src/vtt_class');

const HeaderLine = require('../src/vtt_class').headerLine
const Cue = require('../src/vtt_class').cue
const VttInfo = require('../src/vtt_class').vttInfo

function createPrettyJSON(obj) {
  return JSON.stringify(obj, null, 2)
}

function assertEqual(actualObj, expectedObj) {
  assert.equal(createPrettyJSON(actualObj), createPrettyJSON(expectedObj))
}


describe("dumbest invalid vtt test", function () {
  let fileContent = ``
  let lines = fileContent.split(/\r?\n/) //TODO: look up regexes
  let actual = parseVTT(lines);
  it("should return null for an empty file", function () {
    let expected = null;
    assertEqual(actual, expected)
  })
})

describe("dumbest valid vtt test", function () {
  let fileContent =
    `WEBVTT`
  let lines = fileContent.split(/\r?\n/)
  let actual = parseVTT(lines);
  it("should return a valid but empty VTT model for the most basic VTT file", function () {
    let expected = new VttInfo(new HeaderLine(""), []);
    assertEqual(actual, expected)
  })
})



describe("Sample 1", function () {

  //read lines from file
  /* https://stackoverflow.com/questions/6156501/read-a-file-one-line-at-a-time-in-node-js */
  let fileBuffer = fs.readFileSync('sample_1.vtt', 'utf8');
  // let fileContent = fileBuffer.toString()

  let fileContent =
    `WEBVTT

    00:01.000 --> 00:04.000
    - Never drink liquid nitrogen.
    
    00:05.000 --> 00:09.000
    - It will perforate your stomach.
    - You could die.`

  let lines = fileContent.split(/\r?\n/) //TODO: look up regexes

  //set up test case for sample_1.vtt
  let testOneCueOne = new Cue("00:01.000", "00:04.000", "Never drink liquid nitrogen. ");
  let testOneCueTwo = new Cue("00:05.000", "00:09.000", "It will perforate your stomach. You could die. ")
  let testHeaderOne = new HeaderLine("WEBVTT");
  let cueArray = [testOneCueOne, testOneCueTwo];
  let expected = new VttInfo(testHeaderOne, cueArray);


  describe("Test 1", function () {
    it('sample_1.vtt json matches expected json', function () {
      let actual = parseVTT(lines);

      assertEqual(actual, expected)
    })
  })
})

// describe("Sample 2", function () {

//   //read lines from file
//   /* https://stackoverflow.com/questions/6156501/read-a-file-one-line-at-a-time-in-node-js */
//   let fileBuffer = fs.readFileSync('sample_1.vtt', 'utf8');
//   // let fileContent = fileBuffer.toString()

//   let fileContent =
//     `WEBVTT - This file has cues.

//   14
//   00:01:14.815 --> 00:01:18.114
//   - What?
//   - Where are we now?

//   15
//   00:01:18.171 --> 00:01:20.991
//   - This is big bat country.

//   16
//   00:01:21.058 --> 00:01:23.868
//   - [ Bats Screeching ]
//   - They won't get in your hair. They're after the bugs.`

//   let lines = fileContent.split(/\r?\n/) //TODO: look up regexes

//   describe("Test 1", function () {
//     it('sample_2.vtt json matches expected json', function () {
//       let actual = parseVTT(lines);
//       let expected = testOneJSON;

//       assert.equal(actual, expected)
//     })
//   })
// })